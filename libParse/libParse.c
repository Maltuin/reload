/*
** libList.c for reload in /home/maxime/reload/libParse
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#include "../main.h"

int nbOpt(int nbEl, char *argv[], protoList_t *list)
{
	int count;
	int i;
	int test;

	i = 1;
	count = 0;
	while (i < nbEl)
	{
		if (isInList(list, argv[i]) != 0)
		{
			count++;
		}
		++i;
	}
	return (count);
}

void getOpt(myArg ***args, int nbopt, int argc, char *argv[], protoList_t *list)
{
	int i;
	int j;

	j = 0;
	*args = malloc((nbopt) * sizeof(**args));
	while (i < argc && args != NULL)
	{
		if (isInList(list, argv[i]) != 0)
		{
			(*args)[j] = malloc(sizeof(***args));
			(*args)[j]->key = argv[i];
			if (i < argc-1 && *argv[i+1] != '-')
			{
				(*args)[j]->value = argv[i+1];
				++i;
			}
			++j;
		}
		++i;
	}
}