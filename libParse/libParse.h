/*
** libParse.h for reload in /home/maxime/reload/libParse
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#ifndef libParse_H_
#define libParse_H_

typedef struct
{
    char* key;
    char* value;
}   myArg;

int nbOpt(int nbEl, char *argv[], protoList_t *list);
void getOpt(myArg ***args, int nbopt, int argc, char *argv[], protoList_t *list);

#endif