/*
** libeErr.c for reload in /home/maxime/reload/libErr
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#include "../main.h"
//TODO
//verifier la norme
//verifier les malloc

int printLog(char *str)
{
    int count;
    int logs;

    count = 0;
    
    logs = open("./error.txt",  O_RDWR | O_CREAT | O_APPEND, S_IRWXO | S_IRWXU | S_IRWXG);
    if (logs != 0)
    {
        count = my_putfile(logs, str);
        close(logs);
    }
    return (count);
}

DIR *myopendir(char *path)
{
    errno = 0;
    DIR* rep;

    rep = NULL;
    rep = opendir(path);

    if (errno != 0)
    {
        printLog(strerror(errno));
    }
    return rep;
}

void myclosedir(DIR *dir)
{
    errno = 0;
    closedir(dir);

    if (errno != 0)
    {
        printLog(strerror(errno));
    }
}