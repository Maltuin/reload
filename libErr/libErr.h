/*
** libeErr.h for reload in /home/maxime/reload/libErr
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#ifndef libErr_H_
#define libErr_H_

int printLog(char *str);
DIR *myopendir(char *path);
void myclosedir(DIR *dir);
//libErr_H_
#endif