/*
** libList.h for reload in /home/maxime/reload/libList
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#ifndef libList_H_
#define libList_H_

typedef struct protoList_s
{
    char *arg;
    void (*fonction)(void);

    struct protoList_s *nxt;
} protoList_t;

// createList.c
protoList_t	*createList();
protoList_t *addOnStart(protoList_t *myList, char *arg, void(*func)(void));
protoList_t *addOnEnd(protoList_t *myList, char *arg, void(*func)(void));
protoList_t *deleteList(protoList_t *myList);
protoList_t *deleteOne(protoList_t *myList, char *arg);

//funcOnList.c
int getNbEl(protoList_t *myList);
protoList_t *findOne (protoList_t *myList, char *arg);
int isInList (protoList_t *myList, char *arg);
void echoList(protoList_t *myList);

//libList_H_
#endif