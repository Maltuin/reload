/*
** funcOnList.c for reload in /home/maxime/reload/libList
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#include "../main.h"

int getNbEl(protoList_t *myList)
{
	int nbEl;

	nbEl = 0;
	while (myList)
	{
		nbEl = nbEl + 1;
		myList = myList->nxt;
	}
	return (nbEl);
}

protoList_t *findOne(protoList_t *myList, char *arg)
{
	while (myList)
	{
		if (myStrCmp(myList->arg, arg) == 0)
		{
			return myList;
		}
		myList = myList->nxt;
	}
	return NULL;
}

int isInList(protoList_t *myList, char *arg)
{
	while (myList)
	{
		if (myStrCmp(myList->arg, arg) == 0)
		{
			return 1;
		}
		myList = myList->nxt;
	}
	return 0;
}

void echoList(protoList_t *myList)
{
	while (myList)
	{
		my_putstr(myList->arg);
		myList = myList->nxt;
	}
}