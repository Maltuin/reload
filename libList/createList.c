/*
** createList.c for reload in /home/maxime/reload/libList
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#include "../main.h"

protoList_t	*createList()
{
    protoList_t *myList;

    myList = NULL;
    return (myList);
}

protoList_t *addOnStart(protoList_t *myList, char *arg, void(*func)(void))
{
    protoList_t* newEl;

    newEl = malloc(sizeof(protoList_t));
    if (newEl != NULL) {
	    newEl->arg = arg;
	    newEl->fonction = func;
		newEl->nxt = myList;
		return (newEl);
    }
    else
    {
    	printLog("l'element n'a pas été init");
    }
}

protoList_t *addOnEnd(protoList_t *myList, char *arg, void(*func)(void))
{
	protoList_t* newEl;
	protoList_t* element;
	protoList_t* firstElement;

	firstElement = myList;
	newEl = malloc(sizeof(protoList_t));
	if (newEl != NULL)
	{
		newEl->arg = arg;
		newEl->fonction = func;
		newEl->nxt = NULL;
		if (myList != NULL)
		{
			element = myList;
			while (element->nxt != NULL)
			{
				element = element->nxt;
			}
			element->nxt = newEl;
			return (firstElement);
		}
		return (newEl);
	}
	return (firstElement);
}

protoList_t *deleteList(protoList_t *myList)
{
	protoList_t *nxtElement;

	while (myList->nxt != NULL)
	{
		nxtElement = myList->nxt;
		free(myList);
		myList = nxtElement;
	}
	return NULL;
}

protoList_t *deleteOne(protoList_t *myList, char *arg)
{
	protoList_t *firstElement;
	protoList_t *previousElement;

	previousElement = NULL;
	firstElement = myList;
	if (myList == NULL)
	{
		return NULL;
	}
	while (myStrCmp(myList->arg, arg) != 0) {
		previousElement = myList;
		myList = myList->nxt;
	}
	if (myList) {
		if (previousElement != NULL)
			{
				previousElement->nxt = myList->nxt;
			}
			else
			{
				firstElement = myList->nxt;
			}
		free (myList);
	}
	return firstElement;
}