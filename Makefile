CC=gcc

SRC = main.c

OBJ = $(SRC:.c=.o)

CFLAGS = -W -Wall -Werror -pedantic -std=c99 -g

LIBS = -L. -lParse -lErr -lList -lMy 

NAME=test

all: $(NAME)

$(NAME): makechild $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LIBS) $(CFLAGS)

clean: cleanchild
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)
	rm -rf *.a

re: fclean all

fcleanchild:
	cd libErr && make fclean
	cd libList && make fclean
	cd libMy && make fclean
	cd libParse && make fclean

cleanchild:
	cd libErr && make clean
	cd libList && make clean
	cd libMy && make clean
	cd libParse && make clean

makechild:
	cd libErr && make
	cd libList && make
	cd libMy && make
	cd libParse && make