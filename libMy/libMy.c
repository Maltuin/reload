/*
** libMy.c for reload in /home/maxime/reload/libMy
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#include "../main.h"

int myStrCmp (char *str1, char *str2)
{
	int temp;

	while ((*str1 && *str2) && (*str1++ == *str2++))
		{
			temp = *str1 - *str2;
		};
	return (temp);
}

int my_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		my_putchar(str[i]);
		i++;
	}
	my_putchar('\n');
	return i;
}

int my_putfile(int file, char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		my_putcharfile(file, str[i]);
		i++;
	}
	my_putcharfile(file, '\n');
	return i;
}

void my_putcharfile(int file, char lettre)
{
	write(file, &lettre, 1);
}

void my_putchar(char lettre)
{
	write(1, &lettre, 1);
}