/*
** libMy.h for reload in /home/maxime/reload/libMy
** 
** Made by Maxime MARGAINE
** Login   <margai_m@etna-alternance.net>
** 
** Started on  Mon Feb  22 16:43:16 2015 Maxime MARGAINE
** Last update Mon Feb  22 22:48:29 2013 Maxime MARGAINE
*/
#ifndef libMy_H_
#define libMy_H_

//libMy.c
int myStrCmp (char *str1, char *str2);
int my_putstr(char *str);
void my_putchar(char lettre);
void my_putcharfile(int file, char lettre);
int my_putfile(int file, char *str);

//libMy_H_
#endif